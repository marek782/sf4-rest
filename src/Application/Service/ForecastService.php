<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 01:38
 */

namespace App\Application\Service;


use App\Application\DTO\CityDateForecastDTO;
use App\Factory\ConverterFactoryInterface;
use App\Model\TemperatureScale;
use App\Service\ForecastProviderInterface;

class ForecastService
{
    /**
     * @var ForecastProviderInterface[]
     */
    private $providers = [];

    /**
     * @var ConverterFactoryInterface
     */
    private $converterFactory;

    /**
     * @var TemperatureScale
     */
    private $outputScale;

    /**
     * ForecastService constructor.
     * @param ConverterFactoryInterface $converterFactory
     * @param TemperatureScale $outputScale
     */
    public function __construct(ConverterFactoryInterface $converterFactory, TemperatureScale $outputScale)
    {
        $this->converterFactory = $converterFactory;
        $this->outputScale = $outputScale;
    }


    /**
     * returns forecast for given town at given date
     * finds average forecast from registered providers
     *
     * @param string $town
     * @param string $date
     * @return CityDateForecastDTO
     *
     * todo refactor - to long
     */
    public function getByTownDate(string $town, string $date): CityDateForecastDTO
    {
        $temperatures = [];
        $timeCounters = [];

        // sum up tmeperatures for average
        foreach ($this->providers as $provider) {
            $forecast = $provider->loadForecast($town, $date);

            foreach ($forecast->getTemperatures() as $time => $temperature) {
                $converter = $this->converterFactory->create($temperature->getScale(), $this->outputScale);
                $convertedValue = $converter->convert($temperature)->getValue();

                if (isset($timeCounters[$time])) {
                    $temperatures[$time] += $convertedValue;
                } else {
                    $temperatures[$time] = $convertedValue;
                }

                $timeCounters[$time] = isset($timeCounters[$time]) ? ++$timeCounters[$time] : 1;
            }
        }

        //calculate average
        foreach ($temperatures as $time => $temperature) {
            $temperatures[$time] /= $timeCounters[$time];
        }

        return new CityDateForecastDTO($town, $date, $temperatures, $this->outputScale->getName());
    }

    public function registerProvider(ForecastProviderInterface $provider)
    {
        $this->providers[] = $provider;
    }
}