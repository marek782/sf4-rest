<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 15:55
 */

namespace App\Application\Service;


/**
 * default date validator for forecast endpoint validation purposes
 *
 * Class DateValidator
 * @package App\Application\Service
 */
class DateValidator
{
    const MAX_DAYS_AHEAD = 10;

    /**
     * @param $inputDate
     * @return bool
     */
    public function isValid($inputDate): bool
    {
        $inputTime = strtotime($inputDate);

        $now = time();
        $time = strtotime("+10 days", $now);

        return $time >= $inputTime && $inputTime >= $now;
    }
}