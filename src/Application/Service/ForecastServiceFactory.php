<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 01:38
 */

namespace App\Application\Service;


use App\Application\DTO\CityDateForecastDTO;
use App\Factory\ConverterFactory;
use App\Factory\ConverterFactoryInterface;
use App\Infrastructure\CsvFileForecastProvider;
use App\Infrastructure\XmlFileForecastProvider;
use App\Model\TemperatureScale;
use App\Service\ForecastProviderInterface;

/**
 * Class ForecastServiceFactory
 * @package App\Application\Service
 */
class ForecastServiceFactory
{
    /**
     * initializes forecast application service
     *
     * @param string $scale
     * @param $resourceBaseDir
     * @return ForecastService
     */
    public function create(string $scale, $resourceBaseDir): ForecastService
    {
        $scale = new TemperatureScale($scale);
        $service = new ForecastService(new ConverterFactory(), $scale);
        $service->registerProvider(new CsvFileForecastProvider($resourceBaseDir . '/resources/temps.csv'));
        $service->registerProvider(new XmlFileForecastProvider($resourceBaseDir . '/resources/temps.xml'));

        return $service;
    }
}