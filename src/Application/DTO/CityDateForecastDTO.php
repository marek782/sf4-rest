<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 01:44
 */

namespace App\Application\DTO;


/**
 * Class CityDateForecastDTO
 * @package App\Application\DTO
 */
class CityDateForecastDTO
{
    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $date;

    /**
     * @var array
     */
    private $temperatures;

    /**
     * @var string
     */
    private $format;

    /**
     * TownDateForecastDTO constructor.
     * @param $city
     * @param $date
     * @param $temperatures
     * @param $format
     */
    public function __construct(string $city, string $date, array $temperatures, string $format)
    {
        $this->city = $city;
        $this->date = $date;
        $this->temperatures = $temperatures;
        $this->format = $format;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return array
     */
    public function getTemperatures(): array
    {
        return $this->temperatures;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }
}