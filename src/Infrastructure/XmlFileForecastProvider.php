<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 00:03
 */

namespace App\Infrastructure;


use App\Model\Forecast;
use App\Model\Temperature;
use App\Model\TemperatureScale;
use App\Service\ForecastProviderInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use XMLReader;

class XmlFileForecastProvider implements ForecastProviderInterface
{

    const PREDICTION_DATA_RECORD_NAME = 'prediction';
    const CITY_RECORD_NAME = 'city';
    const DATE_RECORD_NAME = 'date';

    /**
     * @var string
     */
    private $filePath;

    /**
     * @param string $filePath
     * @param array $readOptions determines behaviour of read()
     * @throws FileNotFoundException
     */
    function __construct(string $filePath, array $readOptions = [])
    {

        if (!file_exists($filePath))
            throw new FileNotFoundException("$filePath not found");

        $this->filePath = $filePath;
    }

    /**
     * @param string $place
     * @param string $date
     * @return Forecast
     */
    public function loadForecast(string $place, string $date): Forecast
    {
        $xml = simplexml_load_file($this->filePath);

        $scale = $xml->attributes()->scale;
        $city = $xml->city[0];
        $date = $xml->date[0];

        $temperatures = [];

        foreach ($xml->prediction as $prediction) {
            $temperatures[(string)$prediction->time] = $this->toTemperature($scale, (float)$prediction->value);
        }

        return new Forecast($city, $date, $temperatures);
    }

    private function toTemperature(string $scale, float $value)
    {
        $scale = new TemperatureScale($scale);
        $temperature = new Temperature($scale, $value);

        return $temperature;
    }
}