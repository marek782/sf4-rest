<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 00:03
 */

namespace App\Infrastructure;


use App\Model\Forecast;
use App\Model\Temperature;
use App\Model\TemperatureScale;
use App\Service\ForecastProviderInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use XMLReader;

/**
 * Class CsvFileForecastProvider
 * @package App\Infrastructure
 */
class CsvFileForecastProvider implements ForecastProviderInterface
{

    const CSV_DELIMITER = ',';

    /**
     * @var \SplFileObject
     */
    private $file;

    /**
     * @param string $filePath
     * @throws FileNotFoundException
     */
    function __construct(string $filePath)
    {

        if (!file_exists($filePath))
            throw new FileNotFoundException("$filePath not found");

        $this->file = new \SplFileObject($filePath);
        $this->file->setFlags(\SplFileObject::SKIP_EMPTY);
    }

    /**
     * @param string $place
     * @param string $date
     * @return Forecast
     */
    public function loadForecast(string $place, string $date): Forecast
    {
        //skip columns
        $this->file->fgetcsv(self::CSV_DELIMITER);

        $city = '';
        $date = '';
        $scale = '';
        $temperatures = [];

        while (!$this->file->eof()) {
            $row = $this->file->fgetcsv(self::CSV_DELIMITER);
            $city = !empty($row[1]) ? $row[1] : $city;
            $date = !empty($row[2]) ? $row[2] : $date;
            $scale = !empty($row[0]) ? $row[0] : $scale;
            $temperatures[(string)$row[3]] = $this->toTemperature($scale, (float)$row[4]);
        }

        $forecast = new Forecast($city, $date, $temperatures);

        return $forecast;;
    }

    private function toTemperature(string $scale, float $value)
    {
        $scale = new TemperatureScale($scale);
        $temperature = new Temperature($scale, $value);

        return $temperature;
    }
}