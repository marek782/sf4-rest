<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 09:33
 */

namespace App\Controller;

use App\Application\Service\DateValidator;
use App\Application\Service\ForecastService;
use App\Application\Service\ForecastServiceFactory;
use App\Factory\ConverterFactory;
use App\Infrastructure\CsvFileForecastProvider;
use App\Infrastructure\XmlFileForecastProvider;
use App\Model\TemperatureScale;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ForecastController
 * @package App\Controller
 *
 *
 */
class ForecastController extends FOSRestController implements ClassResourceInterface
{

    private $dateValidator;

    /**
     * @var ForecastServiceFactory
     */
    private $forecastServiceFactory;


    /**
     * ForecastController constructor.
     * @param DateValidator $dateValidator
     * @param $forecastServiceFactory
     */
    public function __construct(DateValidator $dateValidator, ForecastServiceFactory $forecastServiceFactory)
    {
        $this->dateValidator = $dateValidator;
        $this->forecastServiceFactory = $forecastServiceFactory;
    }


    /**
     * @Rest\Get("/forecast/{town}")
     * @Rest\QueryParam(name="date", default="", description="forecast day")
     * @Rest\QueryParam(name="scale", default="celsius", description="forecast scale")
     *
     *
     * route name: "app_forecast_get"
     * [GET] /forecast/{town}
     *
     * @param ParamFetcher $paramFetcher
     * @param $town
     * @return \FOS\RestBundle\View\View
     */
    public function getAction(ParamFetcher $paramFetcher, $town)
    {
        $scale = $paramFetcher->get("scale");
        $service = $this->forecastServiceFactory->create($scale, $this->getParameter('kernel.project_dir'));

        $date = $paramFetcher->get('date');
        $date = $date ?? date('Y-m-d');

        if (!$this->dateValidator->isValid($date)) {
            return $this->view(["error" => 'invalid date cannot be in past and 
                must be max ' . DateValidator::MAX_DAYS_AHEAD . ' days ahead']);
        }

        $result = $service->getByTownDate($town, $date);

        if (count($result->getTemperatures()) == 0) {
            throw new NotFoundHttpException();
        }

        return $this->view($result);
    }

}