<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 02.09.18
 * Time: 21:20
 */

namespace App\Service;


use App\Model\Forecast;

/**
 * abstraction for forecast client services
 *
 * Interface ForecastProviderInterface
 * @package App\Service
 */
interface ForecastProviderInterface
{
    /**
     * @param string $place
     * @param string $date
     * @return Forecast
     */
    public function loadForecast(string $place, string $date): Forecast;
}