<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 01:58
 */

namespace App\Service;


use App\Model\Temperature;
use App\Model\TemperatureScale;

/**
 * Class ToNewInstanceConverter
 * @package App\Service
 */
class ToNewInstanceConverter implements TemperatureConverterInterface
{

    /**
     * @param Temperature $input
     * @return Temperature
     */
    public function convert(Temperature $input): Temperature
    {
        $temperature = new Temperature($input->getScale(), $input->getValue());

        return $temperature;
    }
}