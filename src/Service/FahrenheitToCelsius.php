<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 01:58
 */

namespace App\Service;


use App\Model\Temperature;
use App\Model\TemperatureScale;

/**
 * Class FahrenheitToCelsius
 * @package App\Service
 */
class FahrenheitToCelsius implements TemperatureConverterInterface
{
    const SYMBOL = 'C';
    const CELSIUS_SCALE_NAME = 'Celsius';

    /**
     * @param Temperature $input
     * @return Temperature
     */
    public function convert(Temperature $input): Temperature
    {
        $scale = new TemperatureScale(self::CELSIUS_SCALE_NAME, self::SYMBOL);
        $temperature = new Temperature($scale, $input->getValue() - self::FAHRENHEIT_BASE);

        return $temperature;
    }
}