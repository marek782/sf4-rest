<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 01:58
 */

namespace App\Service;


use App\Model\Temperature;
use App\Model\TemperatureScale;

/**
 * Class CelciusToFahrenheit
 * @package App\Service
 */
class CelciusToFahrenheit implements TemperatureConverterInterface
{
    /**
     *
     */
    const SYMBOL = 'K';

    /**
     * @param Temperature $input
     * @return Temperature
     */
    public function convert(Temperature $input): Temperature
    {
        $scale = new TemperatureScale(self::FAHRENHEIT_BASE, self::SYMBOL);
        $temperature = new Temperature($scale, $input->getValue() + self::FAHRENHEIT_BASE);

        return $temperature;
    }
}