<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 01:55
 */

namespace App\Service;


use App\Model\Temperature;

interface TemperatureConverterInterface
{
    const FAHRENHEIT_BASE = 273.15;

    /**
     * @param Temperature $input
     * @return Temperature
     */
    public function convert(Temperature $input): Temperature;
}