<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 02:29
 */

namespace App\Factory;


use App\Model\TemperatureScale;
use App\Service\TemperatureConverterInterface;

interface ConverterFactoryInterface
{
    /**
     * @param TemperatureScale $input
     * @param TemperatureScale $desired
     * @return TemperatureConverterInterface
     */
    public function create(TemperatureScale $input, TemperatureScale $desired): TemperatureConverterInterface;
}