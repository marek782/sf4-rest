<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 03.09.18
 * Time: 02:31
 */

namespace App\Factory;


use App\Model\TemperatureScale;
use App\Service\CelciusToFahrenheit;
use App\Service\TemperatureConverterInterface;
use App\Service\ToNewInstanceConverter;

/**
 * Class ConverterFactory
 * @package App\Factory
 *
 * todo the Chain of responsibility pattern could replace factory
 *      so collection of converters would be iterated and checked against matching criteria
 *      new converters would be added by registering object in collection
 */
class ConverterFactory implements ConverterFactoryInterface
{

    /**
     * returns appropriate converter instance based on types of
     * given input and desired output scales
     *
     * @param TemperatureScale $input
     * @param TemperatureScale $desired
     * @return TemperatureConverterInterface
     */
    public function create(TemperatureScale $input, TemperatureScale $desired): TemperatureConverterInterface
    {
        if ($this->isCelsiusToFahrenheit($input, $desired)) {
            return new CelciusToFahrenheit();
        }

        if ($this->isFahrenheitToCelsius($input, $desired)) {
            return new CelciusToFahrenheit();
        }

        //by default no converting but returning same value
        return new ToNewInstanceConverter();
    }

    private function isFahrenheitToCelsius(TemperatureScale $input, TemperatureScale $desired): bool
    {
        return
            $input->equals(new TemperatureScale("Celsius"))
            && $desired->equals(new TemperatureScale('Fahrenheit'));
    }

    private function isCelsiusToFahrenheit(TemperatureScale $input, TemperatureScale $desired): bool
    {
        return
            $input->equals(new TemperatureScale("Fahrenheit"))
            && $desired->equals(new TemperatureScale('Celsius'));
    }
}