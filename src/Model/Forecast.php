<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 02.09.18
 * Time: 21:04
 */

namespace App\Model;

/**
 * Class Forecast
 * @package App\Model
 */
class Forecast
{
    /**
     * @var string
     */
    private $town;

    /**
     * @var Temperature[]
     */
    private $temperatures;

    /**
     * @var string
     */
    private $date;

    /**
     * Forecast constructor.
     * @param string $town
     * @param string $date
     * @param Temperature[] $temperatures
     */
    public function __construct(string $town, string $date, array $temperatures)
    {
        // entity must be self validating
        if (!strtotime($date)) {
            throw new \InvalidArgumentException("given $date is not valid date value");
        }

        $this->temperatures = $temperatures;

        if (!$this->validateTemperatures()) {
            throw new \InvalidArgumentException("at least one element of temperature list is not Temperature object");
        }

        $this->town = $town;
        $this->date = $date;
    }


    /**
     * checks if given collection of temperatures contains Temperature object
     *
     * this shouldn't be considered as a validator service but as domain object validation,
     * this is required since no support for generics in php
     */
    private function validateTemperatures(): bool
    {
        foreach ($this->temperatures as $temperature) {
            if (!($temperature instanceof Temperature)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getTown(): string
    {
        return $this->town;
    }

    /**
     * @return Temperature[]
     */
    public function getTemperatures(): array
    {
        return $this->temperatures;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }


}