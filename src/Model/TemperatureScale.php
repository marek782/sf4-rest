<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 02.09.18
 * Time: 20:57
 */

namespace App\Model;


class TemperatureScale
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $symbol;

    /**
     * TemperatureScale constructor.
     * @param string $name
     * @param string $symbol
     */
    public function __construct(string $name, string $symbol = '')
    {
        $this->name = strtolower($name);
        $this->symbol = strtoupper($symbol);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    public function equals(TemperatureScale $scale): bool
    {
        return $scale->getName() == $this->name;
    }
}