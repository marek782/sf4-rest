<?php
/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: 02.09.18
 * Time: 20:57
 */

namespace App\Model;


class Temperature
{

    /**
     * @var TemperatureScale
     */
    private $scale;

    /**
     * @var float
     */
    private $value;

    /**
     * Temperature constructor.
     * @param TemperatureScale $scale
     * @param float $value
     */
    public function __construct(TemperatureScale $scale, float $value)
    {
        $this->scale = $scale;
        $this->value = $value;
    }

    /**
     * @return TemperatureScale
     */
    public function getScale(): TemperatureScale
    {
        return $this->scale;
    }

    /**
     * @return int
     */
    public function getValue(): float
    {
        return $this->value;
    }
}